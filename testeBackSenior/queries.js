const Pool = require('pg').Pool
const pool = new Pool({
  user: 'root',
  host: 'localhost',
  database: 'hotel',
  password: '12345',
  port: 5432,
})

//CRUD HOSPEDE

const getHospedes = (request, response) => {
    pool.query('SELECT * FROM hospede', (error, results) => {
      if (error) {
        throw error
      }
      response.status(200).json(results.rows)
    })
}

const getHospede = (request, response) => {
    const id = parseInt(request.params.id)

    pool.query('SELECT * FROM hospede WHERE id = $1', [id], (error, results) => {
      if (error) {
        throw error
      }
      response.status(200).json(results.rows)
    })
}

const createHospede = (request, response) => {
    const { nome, documento, telefone } = request.body
  
    pool.query('INSERT INTO hospede (nome, documento, telefone) VALUES ($1, $2, $3)', [nome, documento, telefone], (error, results) => {
      if (error) {
        throw error
      }
      response.status(201).json(results)
      //`Hospede cadastrado: ${results.id}`
    })
}

const updateHospede = (request, response) => {
    const id = parseInt(request.params.id)
    const { nome, documento, telefone } = request.body
  
    pool.query(
      'UPDATE hospede SET nome = $1, documento = $2, telefone = $3 WHERE id = $4',
      [nome, documento, telefone, id],
      (error, results) => {
        if (error) {
          throw error
        }
        response.status(200).send(`Hospede atualizado ID: ${id}`)
      }
    )
}

const deleteHospede = (request, response) => {
    const id = parseInt(request.params.id)
  
    pool.query('DELETE FROM hospede WHERE id = $1', [id], (error, results) => {
      if (error) {
        throw error
      }
      response.status(200).send(`Hospede deletado ID: ${id}`)
    })
}

const searchHospede = (request, response) => {
  const search = request.params.search

  pool.query('SELECT h.nome, h.documento, h.telefone, (SELECT SUM(date(datasaida) - date(dataentrada)) AS qtdDias FROM checkin AS c WHERE c.idhospede = h.id) AS totalcheckin, (SELECT DISTINCT date(c.datasaida) - date(c.dataentrada) FROM checkin AS c WHERE c.idhospede = h.id AND c.datasaida = (SELECT MAX(datasaida) FROM checkin AS c WHERE c.idhospede = h.id)) AS ultimocheckin FROM hospede AS h WHERE h.nome LIKE $1 OR h.documento LIKE $1 OR h.telefone LIKE $1', ["%"+search+"%"], (error, results) => {
    if (error) {
      throw error
    }

    const dados = []

    for(i=0; i<results.rows.length; i++){
      dados.push(
        {
          hospede : {
            nome : results["rows"][i]["nome"],
            documento : results["rows"][i]["documento"],
            telefone : results["rows"][i]["telefone"],
            valorTotal: "R$ " + parseInt(results["rows"][i]["totalcheckin"]) * 120 + ",00",
            ultimoCheckin: "R$ " + results["rows"][i]["ultimocheckin"] * 120 + ",00"
          },
        }
      )
    }
    
    response.status(200).send(dados)
  })
}

//CHECK IN
const allCheckin = (request, response) => {

  pool.query('SELECT h.nome, h.documento, h.telefone, (DATE(c.datasaida) - DATE(c.dataentrada)) AS valorTotal FROM checkin AS c INNER JOIN hospede AS h ON c.idhospede = h.id', (error, results) => {
    if (error) {
      throw error
    }

    const dados = []

    for(i=0; i<results.rows.length; i++){
      dados.push(
        {
          nome : results["rows"][i]["nome"],
          documento : results["rows"][i]["documento"],
          telefone : results["rows"][i]["telefone"],
          valorTotal: parseInt(results["rows"][i]["valortotal"]) * 120 + ",00"
        }
      )
    }
    
    response.status(200).send(dados)
  })
}

const doCheckin = (request, response) => {
  const id = parseInt(request.params.id)
  const { dataEntrada, dataSaida, adicionalVeiculo } = request.body

  pool.query('INSERT INTO checkin (dataentrada, datasaida, adicionalveiculo, idhospede) VALUES ($1, $2, $3, $4)', [dataEntrada, dataSaida, adicionalVeiculo, id], (error, results) => {
    if (error) {
      throw error
    }
    response.status(200).send(`Check in realizado com sucesso`)
  })
}

const searchCheckin = (request, response) => {
  const search = request.params.search

  pool.query('SELECT h.nome, h.documento, h.telefone, c.dataentrada, c.datasaida, c.adicionalveiculo FROM checkin AS c INNER JOIN hospede AS h ON c.idhospede = h.id WHERE h.nome LIKE $1 OR h.documento LIKE $1 OR h.telefone LIKE $1', ["%"+search+"%"], (error, results) => {
    if (error) {
      throw error
    }

    const dados = []

    for(i=0; i<results.rows.length; i++){
      dados.push(
        {
          hospede : {
            nome : results["rows"][i]["nome"],
            documento : results["rows"][i]["documento"],
            telefone : results["rows"][i]["telefone"]
          },
          dataEntrada :  results["rows"][i]["dataentrada"],
          dataSaida :  results["rows"][i]["datasaida"],
          adicionalVeiculo:  results["rows"][i]["adicionalveiculo"],
        }
      )
    }
    
    response.status(200).send(dados)
  })
}

const searchHospedado = (request, response) => {
  const hospedado = request.params.hospedado
  let param = "IN"

  if (hospedado == 1) {
    param = "IN"
  } else {
    param = "NOT IN"
  }

  pool.query('SELECT * FROM hospede WHERE id '+ param +' (SELECT DISTINCT idhospede FROM checkin WHERE datasaida > CURRENT_DATE AND dataentrada < CURRENT_DATE)', (error, results) => {
    if (error) {
      throw error
    }

    response.status(200).json(results.rows)
  })
}

module.exports = {
    getHospedes,
    getHospede,
    createHospede,
    updateHospede,
    deleteHospede,
    searchHospede,
    allCheckin,
    doCheckin,
    searchCheckin,
    searchHospedado
}