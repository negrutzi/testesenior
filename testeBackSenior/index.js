'Access-Control-Allow-Origin', '*'
'Access-Control-Allow-Methods', 'GET,POST,OPTIONS,DELETE,PUT'

const express = require('express')
const bodyParser = require('body-parser')
const app = express()
const db = require('./queries')
const port = 3000

app.use(bodyParser.json())
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
)

app.get('/', (request, response) => {
    response.json({ info: 'Node.js, Express, and Postgres API' })
})

//HOSPEDE
app.get('/hospedes', db.getHospedes) //GET TODOS OS HOSPEDES
app.get('/hospede/:id', db.getHospede) //GET UM HOSPEDE
app.post('/hospede', db.createHospede) //SALVAR HOSPEDE
app.put('/hospede/:id', db.updateHospede) //EDITAR HOSPEDE
app.delete('/hospede/:id', db.deleteHospede) //DELETAR HOSPEDE
app.get('/hospede/search/:search', db.searchHospede) //PESQUISAR HOSPEDE POR NOME OU DOCUMENTO OU TELEFONE

//CHECK IN
app.get('/checkin', db.allCheckin) //CONSULTAR TODOS OS CHECKINS
app.post('/checkin/:id', db.doCheckin) //FAZER CHECKIN
app.get('/checkin/:search', db.searchCheckin) //FAZER PESQUISA DE CHECKIN POR NOME OU DOCUMENTO OU TELEFONE DO HOSPEDE
app.get('/checkin/hospedado/:hospedado', db.searchHospedado) //GET HOSPEDES, SE TIVER HOSPEDADO, ENVIAR 1 COMO PARAMETRO, SE NÃO ENVIAR 0

app.listen(port, () => {
    console.log(`App running on port ${port}.`)
})