import { Component, OnInit } from '@angular/core';
import { CrudService } from './services/crud.service';
import  *  as  dados  from  './data/data.json';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  erro: any;
  data: any;

  constructor(private crudService: CrudService){}
  ngOnInit(){
    this.getHospedes();
  }

  getHospedes(){
    this.data = this.crudService.getHospedes();
  }


  title = 'teste';
}
